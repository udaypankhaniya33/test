
from django.db.models import fields
from rest_framework import serializers
from models import menu

  
class MenuSerializer(serializers.ModelSerializer):
    
    
    class Meta:
        managed = False
        db_table = 'menu_menu'
