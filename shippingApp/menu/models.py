from django.db import models

# Create your models here.

class menu(models.Model):
    menuId = models.BigAutoField(primary_key=True)
    mainMenuId=models.IntegerField()
    menuName=models.CharField(max_length=50)
    path=models.CharField(max_length=100)
    icon=models.CharField(max_length=100)
    order=models.IntegerField()    
    add=models.CharField(max_length=4)
    edit=models.CharField(max_length=4)
    view=models.CharField(max_length=4)
    visibility=models.CharField(max_length=4)
    delete=models.CharField(max_length=4)
    addPath=models.CharField(max_length=100)
    deleteMenu=models.CharField(max_length=100)    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)