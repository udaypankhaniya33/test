from django import template
import base64

register = template.Library()

@register.filter(name="encoder")
def  encoder(value):
    
    sample_string_bytes = value.encode("ascii")
    
    base64_bytes = base64.b64encode(sample_string_bytes)
    base64_string = base64_bytes.decode("ascii")
    
    return base64_string


@register.filter
def  decoder(value):
    base64_bytes = value.encode("ascii")
    sample_string_bytes = base64.b64decode(base64_bytes)
    sample_string = sample_string_bytes.decode("ascii")


    return sample_string
