"""shippingApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from shippingApp import views
from django.conf.urls.static import static
from django.conf import settings
from content import views as Cv
from helpandsupport import views as vH
from customers import views as viewCs
from  orders import views as oV



base64_pattern = r'(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$'
urlpatterns = [

# all admin functions 
	path('admin/', admin.site.urls),
	path('home/', views.home),
	path('', views.login),
	path('islogin/', views.islogin),
	path('logout', views.userlogout),
	path("Edit-Profile/",views.editprofile),
	path("updateAdmin",views.updateAdmin),
	path('UpdatePassword/',views.updatePassword),
	path("Change-Password/",views.changePassword),
 	path('Forget-Password/',views.forgetPassword),
	path('generate/',views.generateotp),


#users 
	path('View-User/<str:CustomerId>',views.editUser),
	path('Users/',views.users),
	path("All-customers/",viewCs.getCustomers),
	path("Block_UnBlock_User/",viewCs.blockUnblock),
	path("delete_User/",viewCs.deleteCustomers),
	path("Pending-Driver-Request/",viewCs.Pending_Drivers),
	path("Reject-Driver-Request/",viewCs.Reject_Drivers),
	path("Reject-Request/",viewCs.RejectDriverRequest),
	path("Approve-Driver-Request/",viewCs.ApproveDriverRequest),
    path("All-Dirvers/",viewCs.getDrivers),


#order 
    path("All-Orders/",oV.getAllOrders),
    # runningOrders
    path("runningOrders/",oV.runningOrders),
    path("Pending-Orders/",oV.pendingOrders),
    # path("Complete-Orders/",oV.CompletedOrders),
    
    path("Order-Details/<str:orderId>",oV.orderDetail),
    path("Order-chats/<str:orderId>",oV.orderChats),
    
    
    
    
#ajax
	 path("getChats/",oV.getChats),






#other pages
	path('Terms-And-Conditions/',Cv.getTandC),
	path('UpdateTC/',Cv.UpdateTC),
	path('Help-And-Support/',vH.InquiryPage),
	
	
	
#apiPaths


  path("customersignin/",viewCs.customer_signin),
    path("customersignup/",viewCs.customer_signup),
    path("forgotpassword/",viewCs.forgotpassword),
    path("verifyotp/",viewCs.verify_otp),
    path("resetpassword/",viewCs.reset_password),
    path("viewprofile/",viewCs.view_profile),
    path("editprofile/",viewCs.edit_profile),

 

]



# only in development
if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
