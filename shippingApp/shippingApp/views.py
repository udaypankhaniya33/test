from cProfile import Profile
# from curses import reset_prog_mode
from datetime import date
from distutils.log import error
import email
from multiprocessing import context
import os
from sys import setprofile
from urllib import response
from attr import field
from django.http import Http404, HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.core import serializers
from accounts.models import Admin_account
from customers.models import customers
from menu.models import menu
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_serverside_datatable.views import ServerSideDatatableView
import requests
import json
import base64
import hashlib
from django import template
from orders.models import orders
from shippingApp.settings import BASE_DIR
import random
import math
import datetime
basePath = "http://216.10.247.209:8080/api/admin/"

# returns all admin and menudata


def DefaultData(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
            query = menu.objects.raw(
                'SELECT *  FROM menu_menu WHERE "mainMenuId"=\'0\'')
            data = []
            menuData = []
            userData = []
            json_data = json.loads(serializers.serialize("json", query))
            userObject = Admin_account.objects.filter(id=accountId)
            UserJs = json.loads(serializers.serialize("json", userObject))

            for j in UserJs:
                j["fields"]["userId"] = (json.loads(json.dumps(j["pk"])))
                userData.append(json.loads(json.dumps(j["fields"])))

            for j in json_data:
                j["fields"]["manuId"] = (json.loads(json.dumps(j["pk"])))
                j["fields"]["subMenu"] = []
                data.append(json.loads(json.dumps(j["fields"])))

            for d in data:
                manu = json.loads(json.dumps(d))
                subquery = menu.objects.filter(mainMenuId=manu["manuId"])
                json_data = json.loads(serializers.serialize("json", subquery))
                submenu = []
                for j in json_data:
                    j["fields"]["manuId"] = (json.loads(json.dumps(j["pk"])))
                    subs = json.loads(json.dumps(j["fields"]))
                    submenu.append(subs)
                    d["subMenu"] = submenu
                menuData.append(json.loads(json.dumps(d)))
            jsondata = {"data": menuData,
                      "userData": userData}
            # print(jsondata)

            return jsondata
    else:
            return jsondata


def home(request):
    


	accountId = request.session.get("accountId")
	if accountId is not None:
		userCounts = customers.objects.filter(type=0, delete=0).count()
		driverCounts = customers.objects.filter(type=1, delete=0).count()
		pendingRequestCount = customers.objects.filter(
		type=0, delete=0, isRequested=1).count()
		allOrdersCount = orders.objects.filter(delete=0).count()
		runningOrdersCount = orders.objects.filter(delete=0, OrderStatus=1).count()
		deliverdOrdersCount = orders.objects.filter(delete=0, OrderStatus=2).count()

		data = DefaultData(request)
		data["DashboardData"] = [{
			"userCount": userCounts,
			"driverCount": driverCounts,
			"pendingRequestCount": pendingRequestCount,
			"allOrders": allOrdersCount,
			"runningOrdersCount": runningOrdersCount,
			"deliverdOrders": deliverdOrdersCount
		}]
		# print(data)
		return render(request, "index.html", data)
	else:
			return redirect("/")


def login(request):
	accountId = request.session.get("accountId")
	if accountId is None:
		return render(request, "login.html")
	else:
		return redirect('home/')


def islogin(request):
	accountId = request.session.get("accountId")
	if accountId is None:

		if request.method == 'POST':
			var = json.loads(json.dumps(request.POST))

			data = json.loads(var["data"])

			userNumber = data["number"]
			password = data["password"]

			h = hashlib.md5(password.encode()).hexdigest()

		try:
			userObject = Admin_account.objects.get(number=userNumber)
			if userObject.password == h:
				request.session["accountId"] = userObject.id
				return HttpResponse("1")
			else:

				return HttpResponse("2")
		except:

			return HttpResponse("3")

	else:
		return HttpResponse("3")


def editUser(request, CustomerId):
	accountId = request.session.get("accountId")

	if accountId is not None:

		mainData = []
		OrderData = []
		base64_bytes = CustomerId.encode("ascii")
		sample_string_bytes = base64.b64decode(base64_bytes)
		sample_string = (sample_string_bytes.decode("ascii"))
		# print(sample_string)

		userObject = customers.objects.filter(id=sample_string)

		UserJs = json.loads(serializers.serialize("json", userObject))
		for j in UserJs:
			# print(j)
			mainData.append(json.loads(json.dumps(j["fields"])))
		reusable  = DefaultData(request)
		reusable ["mainData"] = mainData
		userObject = orders.objects.filter(delete=0,userId=sample_string)
		UserJs = json.loads((serializers.serialize("json", userObject)))
		for j in UserJs:

			j["fields"]['userId'] = (json.dumps(j["fields"]['userId']))

			userPic = customers.objects.filter(id=j["fields"]['userId'])
			UserJs = json.loads((serializers.serialize("json", userPic)))

			for k in UserJs:
				j["fields"]["userpic"] = json.loads(
					json.dumps(k["fields"]['profilepic']))
				# mainData.append(json.loads(json.dumps(j["fields"])))

			j["fields"]['orderId'] = (json.dumps(j['pk']))
			j["fields"]['transporterId'] = (
				json.dumps(j["fields"]['transporterId']))
			transporterPic = customers.objects.filter(
				id=j["fields"]['transporterId'])
			UserJs = json.loads(
				(serializers.serialize("json", transporterPic)))

			for k in UserJs:

				j["fields"]["transporterpic"] = json.loads(
					json.dumps(k["fields"]['profilepic']))
			

			OrderData.append(json.loads(json.dumps(j["fields"])))




		reusable["OrderData"] = OrderData
		print(json.dumps(reusable["OrderData"]))

		return render(request, "userDetails.html", reusable )
	else:
		return redirect("/")


def changePassword(request):
	accountId = request.session.get("accountId")
	if accountId is not None:
		try:
			json_data = DefaultData(request)

			return render(request, "changepassword.html", json_data)
		except:
			return render(request, "changepassword.html", json_data)

	else:
		return redirect("/")


def updatePassword(request):

	accountId = request.session.get("accountId")
	if accountId is not None:
		if request.method == 'POST':
			password = request.POST["currentPassword"]
			Newpassword = request.POST["newPassword"]
			new = hashlib.md5(Newpassword.encode()).hexdigest()
			old = hashlib.md5(password.encode()).hexdigest()
			try:
				userObject = Admin_account.objects.get(id=accountId)
				json_data = DefaultData(request)
				if userObject.password == old:
					error = {"error": {
						"er3": "password Chaneged"
					}}
					json_data.update(error)
					Admin_account.objects.filter(id=accountId).update(password=new)

				
					return render(request, "changepassword.html", json_data)
				else:
					error = {"error": {
						"er2": "invalid user"
					}}
					json_data.update(error)
					return render(request, "changepassword.html", json_data)

			except:
			
				return render(request, "changepassword.html", json_data)

	else:
		return redirect("/")


def editprofile(request):

	accountId = request.session.get("accountId")
	if accountId is not None:
		
		json_data = DefaultData(request)

		return render(request, "editprofile.html", json_data)
	else:
		return redirect("/")


def userlogout(request):
	accountId = request.session.get("accountId")
	try:
		del request.session["accountId"]
		return redirect("/")
	except:
		return redirect("/")

def updateAdmin(request):

	accountId = request.session.get("accountId")

	if request.method == 'POST':

		firstName = request.POST["firstName"]
		lastName = request.POST["lastName"]
		email = request.POST["email"]
		mobileNumber = request.POST["mobileNumber"]
		profile = request.FILES.get('profile')
		
	
		
  
		if profile:
			try:
				try:
					fs = FileSystemStorage(location="static/media/")
					file = fs.save(profile.name, profile)
				except:
					return redirect('home/')
				finally:

					ext="."+(file).split(".")[-1]
					date=datetime.datetime.now().strftime("%I-%M-%p-%B-%d-%Y")
					temp=date+ext
					os.rename( "static/media/"+file ,"static/media/"+temp)
					print(file)
	
				Admin_account.objects.filter(id=accountId).update(
					firstname=firstName, lastname=lastName, email=email, number=mobileNumber, profilepic=temp)
				return redirect('Edit-Profile/')
			except:

		
				return redirect('home/')
		else:
			Admin_account.objects.filter(id=accountId).update(
				firstname=firstName, lastname=lastName, email=email, number=mobileNumber)
			return redirect('Edit-Profile/')

	else:

		return redirect("Edit-Profile/", {"error": "Somthing went wrong"})

def users(request):
	accountId = request.session.get("accountId")
	if accountId is not None:
		mainData=[]		
		userObject=customers.objects.raw(' SELECT * FROM customers_customers WHERE  "delete"=0 ')

		UserJs =json.loads( (serializers.serialize( "json",userObject)))
		# print(UserJs)
		for j in UserJs:
			# print(j)
			j["fields"]["id"]=(json.dumps(j["pk"]))
			mainData.append(json.loads(json.dumps(j["fields"])))
			
		reusable =DefaultData(request)
		reusable ["mainData"]=mainData
        # print(reusable )

		json_data =reusable 



		return render(request, "users.html", json_data)
	else:
		return redirect("/")


def forgetPassword(request):
    
	return render(request, "forgotPassword.html")

def generateotp(request):
    
	if request.method == 'POST':
		var= json.loads(json.dumps(request.POST))
		data=json.loads(var["data"])
		email=data["email"]
		OTP = random.randint(1111,9999)

		try:
			userObject = Admin_account.objects.filter(email=email).count()
			if userObject>0:
       
				Admin_account.objects.filter(email=email).update(otp=OTP)
				json_data="OTP is Generated Successfully"
			else:
				json_data="This user does not exist"
				
   
		except:
  
				json_data="Somthing Went Wrong"

      
		if json_data=="This user does not exist":
			return HttpResponse("2") 
		elif json_data=="OTP is Generated Successfully":
			return HttpResponse("1") 
		elif json_data=="Somthing Went Wrong":
			return HttpResponse("3") 
		else:
			return HttpResponse("3") 
	else:
		return HttpResponse("3") 

