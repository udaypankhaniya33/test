
from django.db import models


# Create your models here.
class chatDetails(models.Model):
    
    chatId=models.IntegerField()  
    senderId=models.IntegerField()
    receiverId=models.IntegerField()
    senderType = models.CharField(max_length=255, blank=True, null=True)
    receiverType = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    delete=models.SmallIntegerField(default='0')    