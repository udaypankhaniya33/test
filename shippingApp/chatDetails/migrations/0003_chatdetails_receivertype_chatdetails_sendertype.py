# Generated by Django 4.0.1 on 2022-03-12 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatDetails', '0002_rename_orderid_chatdetails_chatid'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatdetails',
            name='receiverType',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='chatdetails',
            name='senderType',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
