from django.db import models

# Create your models here.

class Admin_account(models.Model):
        firstname = models.CharField(db_column='firstName', max_length=50) # Field name made lowercase.
        password = models.CharField(max_length=50)
        number = models.CharField(max_length=15)
        created = models.DateTimeField(blank=True, null=True)
        updated = models.DateTimeField(blank=True, null=True)
        profilepic = models.CharField(db_column='profilePic', max_length=100, blank=True, null=True) # Field name made lowercase.
        lastname = models.CharField(db_column='lastName', max_length=100, blank=True, null=True) # Field name made lowercase.
        email = models.CharField(max_length=100, blank=True, null=True)
        otp = models.CharField(max_length=4, blank=True, null=True)
