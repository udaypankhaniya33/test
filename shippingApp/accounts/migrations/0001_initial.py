# Generated by Django 4.0.1 on 2022-02-15 13:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin_account',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(db_column='firstName', max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('number', models.CharField(max_length=15)),
                ('created', models.DateTimeField(blank=True, null=True)),
                ('updated', models.DateTimeField(blank=True, null=True)),
                ('profilepic', models.CharField(blank=True, db_column='profilePic', max_length=100, null=True)),
                ('lastname', models.CharField(blank=True, db_column='lastName', max_length=100, null=True)),
                ('email', models.CharField(blank=True, max_length=100, null=True)),
                ('otp', models.CharField(blank=True, max_length=4, null=True)),
            ],
        ),
    ]
