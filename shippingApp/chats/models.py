from django.db import models

# Create your models here.


class chat(models.Model):
    orderId=models.IntegerField()  
    userId=models.IntegerField()
    transporterId=models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    delete=models.SmallIntegerField(default='0')    