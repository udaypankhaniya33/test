from cProfile import Profile
from distutils.log import error
from email import message
from multiprocessing import context
from sre_constants import SUCCESS
from sys import setprofile
from warnings import catch_warnings
from attr import field
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.core import serializers
from accounts.models import Admin_account
from content.models import content
# from country.models import country
from customers.models import customers
from menu.models import menu
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_serverside_datatable.views import ServerSideDatatableView
from shippingApp import views
# from state.models import state
import requests
import json
import hashlib

def getTandC(request):

	accountId = request.session.get("accountId")
	if accountId is not None:
		mainData=[]
		userObject=content.objects.filter(key='T&C')
		UserJs =json.loads( (serializers.serialize( "json",userObject)))
		for j in UserJs:
			j["fields"]["id"]=(json.dumps(j["pk"]))
			mainData.append(json.loads(json.dumps(j["fields"])))
			
		reusable =views.DefaultData(request)
		reusable ["mainData"]=mainData
		# print(reusable )

		json_data =reusable 

		return render(request, "turmsandconditions.html",json_data) 
	else:
		return redirect("/")






def UpdateTC(request):

	accountId = request.session.get("accountId")
	if accountId is not None:
		if request.method == 'POST':
			value = request.POST["summernote"]

	
	
			error = {"error": {
				"er3": "password Chaneged"
			}}
		
			content.objects.filter(key='T&C').update(value=value)
			mainData=[]
			userObject=content.objects.filter(key='T&C')
			UserJs =json.loads( (serializers.serialize( "json",userObject)))
			for j in UserJs:
				j["fields"]["id"]=(json.dumps(j["pk"]))
				mainData.append(json.loads(json.dumps(j["fields"])))
				
			reusable =views.DefaultData(request)
			reusable ["mainData"]=mainData

			# print(reusable )

			json_data =reusable 
			json_data.update(error)		

			return render(request, "turmsandconditions.html",json_data) 
	else:
		return redirect("/")