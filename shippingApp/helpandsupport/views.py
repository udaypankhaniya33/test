from django.shortcuts import render
from helpandsupport.models import helpandsupport

from cProfile import Profile
from distutils.log import error
from multiprocessing import context
from sys import setprofile
from attr import field
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.core import serializers
from accounts.models import Admin_account
from customers.models import customers
from menu.models import menu
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_serverside_datatable.views import ServerSideDatatableView
import requests
import json
import hashlib
from shippingApp import views


from shippingApp.settings import BASE_DIR
basePath="http://216.10.247.209:8080/api/admin/"



# SELECT * FROM public.helpandsupport_helpandsupport LEFT JOIN public.customers_customers ON helpandsupport_helpandsupport."customerId"=customers_customers.id 


def InquiryPage(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
        # return render(request, "index.html")
 
        mainData=[]

        
        dataObject=helpandsupport.objects.raw('SELECT * FROM public.helpandsupport_helpandsupport LEFT JOIN public.customers_customers ON helpandsupport_helpandsupport."customerId"=customers_customers.id')
        
        dataResult =json.loads( (serializers.serialize( "json",dataObject)))
        # print(UserJs)

        for j in dataResult:

            userData=customers.objects.filter(id=j["fields"]['customerId'])      
            UserJs =json.loads( (serializers.serialize("json",userData)))


            for k in UserJs:
          
                j["fields"]["firstname"]=json.loads(json.dumps(k["fields"]['firstname']))
                j["fields"]["lastname"]=json.loads(json.dumps(k["fields"]['lastname']))
                j["fields"]["profilepic"]=json.loads(json.dumps(k["fields"]['profilepic']))
                
            mainData.append(json.loads(json.dumps(j["fields"])))
                
        reusable =views.DefaultData(request)
        reusable ["mainData"]=mainData

        json_data =reusable 
        
        return render(request, "HelpAndSupport.html", json_data)
    else:
        return redirect("/")

