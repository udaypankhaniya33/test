import email
from django.db import models

# Create your models here.


class customers(models.Model):

    lastname= models.CharField( max_length=50,blank=True, null=True)
    firstname = models.CharField( max_length=50,blank=True, null=True)
    password = models.CharField(max_length=50)
    number = models.CharField(max_length=15)
    email=models.CharField(max_length=100)
    otp=models.IntegerField()
    profilepic = models.CharField( max_length=100, blank=True, null=True)
    type= models.SmallIntegerField(default='0') 
    # type = 0 for customer , type=1 for driver , 
    allowNotification=models.SmallIntegerField(default='1')
    isRequested=models.SmallIntegerField(default=0)
    # 0 not requested , 1 pending , 2 approved , 3 rejected    
    status= models.SmallIntegerField(default='0',blank=True, null=True)    
    created = models.DateTimeField(auto_now_add= True)
    updated = models.DateTimeField(auto_now_add= True)
    delete = models.SmallIntegerField(default='0')
    rejectionreason = models.TextField(db_column='rejectionReason', blank=True, null=True)
    document=models.CharField(blank=True, max_length=200, null=True)
    dob = models.DateField(blank=True, null=True)
