# Generated by Django 4.0.1 on 2022-03-01 05:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0014_rename_delete_customers_deleted'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customers',
            old_name='deleted',
            new_name='delete',
        ),
    ]
