# Generated by Django 4.0.2 on 2022-02-25 06:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0010_customers_rejectionreason'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customers',
            name='firstname',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='customers',
            name='lastname',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
