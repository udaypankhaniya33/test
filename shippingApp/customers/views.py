from cProfile import Profile
from distutils.log import error
import email
from multiprocessing import context
from sys import setprofile
from attr import field
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.core import serializers
from accounts.models import Admin_account
from customers.models import customers
from menu.models import menu
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_serverside_datatable.views import ServerSideDatatableView

import requests
import json
import hashlib


from shippingApp import views


from shippingApp.settings import BASE_DIR
basePath="http://216.10.247.209:8080/api/admin/"





def getCustomers(request):

    accountId = request.session.get("accountId")
    if accountId is not None:
        
        mainData=[]

        
        userObject=customers.objects.raw(' SELECT * FROM customers_customers WHERE "type"=0 AND "delete"=0 ')
        
        UserJs =json.loads( (serializers.serialize( "json",userObject)))
    

        for j in UserJs:

            j["fields"]["id"]=(json.dumps(j["pk"]))
            mainData.append(json.loads(json.dumps(j["fields"])))
            
        reusable =views.DefaultData(request)
        reusable ["mainData"]=mainData
        # print(reusable )

        json_data =reusable 


        return render(request, "customers.html", json_data)
    else:
        return redirect("/")


def getDrivers(request):

    accountId = request.session.get("accountId")
    if accountId is not None:
        
        mainData=[]

        
        userObject=customers.objects.raw(' SELECT * FROM customers_customers WHERE "type"=1 AND "delete"=0  AND "isRequested"=2')
        
        UserJs =json.loads( (serializers.serialize( "json",userObject)))


        for j in UserJs:

            j["fields"]["id"]=(json.dumps(j["pk"]))
            mainData.append(json.loads(json.dumps(j["fields"])))
            
        reusable =views.DefaultData(request)
        reusable ["mainData"]=mainData
        # print(reusable )

        json_data =reusable 

        return render(request, "drivers.html", json_data)
    else:
        return redirect("/")


def Pending_Drivers(request):

    accountId = request.session.get("accountId")
    if accountId is not None:
        # return render(request, "index.html")

         
        mainData=[]

        
        userObject=customers.objects.raw(' SELECT * FROM customers_customers WHERE "type"=0 AND "delete"=0 AND "isRequested"=1')
        
        UserJs =json.loads( (serializers.serialize( "json",userObject)))
 

        for j in UserJs:

            j["fields"]["id"]=(json.dumps(j["pk"]))
            mainData.append(json.loads(json.dumps(j["fields"])))
            
        reusable =views.DefaultData(request)
        reusable ["mainData"]=mainData
    
        json_data =reusable 

        return render(request, "DriverRequest.html", json_data)
    else:
        return redirect("/")

def Reject_Drivers(request):

    accountId = request.session.get("accountId")
    if accountId is not None:
    
         
        mainData=[]

        
        userObject=customers.objects.raw(' SELECT * FROM customers_customers WHERE "type"=0 AND "delete"=0 AND "isRequested"=3')
        
        UserJs =json.loads( (serializers.serialize( "json",userObject)))
    

        for j in UserJs:

            j["fields"]["id"]=(json.dumps(j["pk"]))
            mainData.append(json.loads(json.dumps(j["fields"])))
            
        reusable =views.DefaultData(request)
        reusable ["mainData"]=mainData
    
        json_data =reusable 
        
       

        return render(request, "RejectedRequest.html", json_data)
    else:
        return redirect("/")




# ajax functions

def blockUnblock(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
         
        if request.method == 'POST':
            mainData=[]
            resusable={}
            resusable["message"]=""
            var= json.loads(json.dumps(request.POST))
            type='0'
            data=json.loads(var["data"])
    
            customersId = data["customersId"]
            userObject= customers.objects.filter(id=customersId)
            UserJs =json.loads( (serializers.serialize( "json",userObject)))

            for j in UserJs:
                type=(json.dumps(j["fields"]["status"]))
            try:           
                
                if type=='0':
                    customers.objects.filter(id=customersId).update(status='1')
                    resusable["message"]="blocked"
                else:    
                    customers.objects.filter(id=customersId).update(status='0')
                    resusable["message"]="Unblocked"
              
            
            except:
                return HttpResponse("3") 
            # return HttpResponse("3") 
    
                # print("exp")
            if resusable["message"]=="Unblocked":
                return HttpResponse("2") 
            elif resusable["message"]=="blocked":  
                return HttpResponse("1") 
            else:
                return HttpResponse("3") 
        else:
            return HttpResponse("3") 
        
        

def deleteCustomers(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
         
        if request.method == 'POST':
            resusable={}
            resusable["message"]=""
            var= json.loads(json.dumps(request.POST))
        
            data=json.loads(var["data"])
                        
            customersId = data["customersId"]
                 
            userObject= customers.objects.filter(id=customersId)
            UserJs =json.loads( (serializers.serialize( "json",userObject)))

            
            try:           
                
     
                    customers.objects.filter(id=customersId).update(delete='1')
                    resusable["message"]="delete"
            
            except:
                return HttpResponse("3") 
            # return HttpResponse("3") 
    
                # print("exp")
            if resusable["message"]=="delete":
           
                return HttpResponse("1") 
            else:
                return HttpResponse("3") 
        else:
            return HttpResponse("3") 
        

def RejectDriverRequest(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
         
        if request.method == 'POST':
            resusable={}
            resusable["message"]=""
            var= json.loads(json.dumps(request.POST))
        
            data=json.loads(var["data"])
                        
            value = data["value"]
            customersId = data["CustomerId"]
            
            userObject= customers.objects.filter(id=customersId)
            UserJs =json.loads( (serializers.serialize( "json",userObject)))

            try:           
                
                customers.objects.filter(id=customersId).update(rejectionreason=value,type='0',isRequested='3')
                resusable["message"]="Success"
            
            except:
                    resusable["message"]="failed"
                    return HttpResponse("3") 
        
                 
           

            if resusable["message"]=="failed":
                return HttpResponse("2") 
            elif resusable["message"]=="Success":  
                return HttpResponse("1") 
            else:
                return HttpResponse("3") 
        else:
            return HttpResponse("3") 
    
    
    
def ApproveDriverRequest(request):
    accountId = request.session.get("accountId")
    if accountId is not None:
         
        if request.method == 'POST':
            resusable={}
            resusable["message"]=""
            var= json.loads(json.dumps(request.POST))
        
            data=json.loads(var["data"])
            customersId = data["CustomerId"]
            
            userObject= customers.objects.filter(id=customersId)
            UserJs =json.loads( (serializers.serialize( "json",userObject)))

            try:           
                
                customers.objects.filter(id=customersId).update(type='1',isRequested='2')
                resusable["message"]="Success"
            
            except:
                    resusable["message"]="failed"
                    return HttpResponse("3") 
        
                 
           

            if resusable["message"]=="failed":
                return HttpResponse("2") 
            elif resusable["message"]=="Success":  
                return HttpResponse("1") 
            else:
                return HttpResponse("3") 
        else:
            return HttpResponse("3") 
            
# apis


import datetime
import math, random
from django.http import JsonResponse

current_datetime = datetime.datetime.now()  



#otp generate function
def generateOTP() :
     digits = "123456789"
     OTP = ""
     for i in range(4) :
         OTP += digits[math.floor(random.random() * 10)]
     return OTP


# LogIn Customer 
def customer_signin(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
            
            data = json.loads(request.body.decode("utf-8"))
            email = data.get('email')
            password = data.get('password')

            if email == '':
                customer ={
                    "status":"400",
                    "message":"Email is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif password == '':
                customer ={
                    "status":"400",
                    "message":"Password is Requried",
                }
                return JsonResponse(customer,safe=False)

            else: 
                try:
                    h = hashlib.md5(password.encode()).hexdigest()
                   
                    userObject = customers.objects.get(delete=0,email=email,status=0)
                    id=userObject.id
                    if userObject.password == h:
                        customer ={
                        "status":"200",
                        "message":"Login Successful",
                        "userid":id,
                        "fullname":userObject.firstname
                        }
                        return JsonResponse(customer,safe=False)
                    else:
                        
                        customer ={
                            "status":"200",
                            "message":"Password is Wrong!"  
                            }
                        return JsonResponse(customer,safe=False)
                except:
                    customer ={
                    "status":"400",
                    "message":"User not found",
                    }
                    return JsonResponse(customer,safe=False)
        else: 
            customer ={
                "status":"200",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

# Customer SignUp
def customer_signup(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
            
            data = json.loads(request.body.decode("utf-8"))
            fullname=data.get('fullname')
            email = data.get('email')
            password = data.get('password')
            confirmpassword=data.get('confirmpassword')
            passwordCompare=(password == confirmpassword)
          
           
            if fullname == '':
                customer ={
                    "status":"400",
                    "message":"Fullname is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif email == '':
                customer ={
                    "status":"400",
                    "message":"Email is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif password == '':
                customer ={
                    "status":"400",
                    "message":"Password is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif confirmpassword == '':
                customer ={
                    "status":"400",
                    "message":"Confirm Password Required",
                }
                return JsonResponse(customer,safe=False)
            elif passwordCompare != True :
               
                customer ={
                    "status":"400",
                    "message":"Password not match...",
                }
                return JsonResponse(customer,safe=False)
            else: 
                try:
                    # print(current_datetime)
                    h = hashlib.md5(password.encode()).hexdigest() #password convert md5
                    userCounts = customers.objects.filter(delete=0,email=email,status=0).count() #count customer data
                   
                    if userCounts > 0 :
                        customer ={
                        "status":"400",
                        "message":"Email is alreay Exits"
                        }
                        return JsonResponse(customer,safe=False)
                    else:

                        try:
                            insertQuery = customers(lastname=fullname,firstname=fullname,email=email,password=h,profilepic="images.jpg")
                            insertQuery.save() #Inasertquery save
                            
                            customer ={
                                    "status":"200",
                                    "message":"Registration Successful"  
                                    }
                            return JsonResponse(customer,safe=False)
                        except:
                           
                            customer ={
                                "status":"400",
                                "message":"Somthing went wrong.."  
                                }
                            return JsonResponse(customer,safe=False)
                except:
                    customer ={
                    "status":"400",
                    "message":"Somthing went wrong..",
                    }
                    return JsonResponse(customer,safe=False)
        else: 
            customer ={
                "status":"200",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

# forgot password
def forgotpassword(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
            
            data = json.loads(request.body.decode("utf-8"))
            email = data.get('email')
            
            if email == '':
                customer ={
                    "status":"400",
                    "message":"Email is Requried",
                }
                return JsonResponse(customer,safe=False)
           
            else: 
                try:
                    # otp generate function call
                    userOtp=generateOTP()
                    userData = customers.objects.filter(delete=0,email=email,status=0)
                    json_data =json.loads(serializers.serialize( "json",userData))
                    # print(json.dumps(json_data['pk']))
                    for j in json_data:
                        userId=json.loads(json.dumps(j['pk']))
                    
                    if id != None:
                        customers.objects.filter(id=userId).update(otp=userOtp)
                        customer ={
                        "status":"400",
                        "message":"Otp Genearte successfully",
                        "userid":userId,
                        "otp":userOtp
                        }
                        return JsonResponse(customer,safe=False)
                    else:
                        customer ={
                                "status":"400",
                                "message":"This user does not exist"  
                                }
                        return JsonResponse(customer,safe=False)
                except:
                    customer ={
                    "status":"400",
                    "message":"Somthing went wrong..",
                    }
                    return JsonResponse(customer,safe=False)
        else: 
            customer ={
                "status":"200",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

#verify_otp
def verify_otp(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
            
            data = json.loads(request.body.decode("utf-8"))
            userid = data.get('userid')
            otp = data.get('otp')
            
            if userid == '':
                customer ={
                    "status":"400",
                    "message":"CustomerId is Requried",
                }
                return JsonResponse(customer,safe=False)
           
            else: 
                try:
                    # otp generate function call
                    userOtp=generateOTP()
                    userData = customers.objects.filter(delete=0,id=userid,status=0,otp=otp)
                    json_data =json.loads(serializers.serialize( "json",userData))
                    # print(json.dumps(json_data['pk']))
                    for j in json_data:
                        userId=json.loads(json.dumps(j['pk']))
                    
                    if id != None:
                        customers.objects.filter(id=userId).update(otp=userOtp)
                        customer ={
                        "status":"400",
                        "message":"Otp is Verify Successfully",
                        
                        }
                        return JsonResponse(customer,safe=False)
                    else:
                        customer ={
                                "status":"400",
                                "message":"OTP Incorrect"  
                                }
                        return JsonResponse(customer,safe=False)
                except:
                    customer ={
                    "status":"400",
                    "message":"UserId and Otp are wrong",
                    }
                    return JsonResponse(customer,safe=False)
        else: 
            customer ={
                "status":"200",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

#reset Password
def reset_password(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
            
            data = json.loads(request.body.decode("utf-8"))
            userid=data.get('userid')
            password = data.get('password')
            confirmpassword=data.get('confirmpassword')
            passwordCompare=(password == confirmpassword)
            if password == '':
                customer ={
                    "status":"400",
                    "message":"Password is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif confirmpassword == '':
                customer ={
                    "status":"400",
                    "message":"Confirm Password Required",
                }
                return JsonResponse(customer,safe=False)
            elif passwordCompare != True :
               
                customer ={
                    "status":"400",
                    "message":"Password not match...",
                }
                return JsonResponse(customer,safe=False)
            else: 
                try:
                    h = hashlib.md5(password.encode()).hexdigest() #password convert md5

                    userData = customers.objects.filter(delete=0,id=userid,status=0).count()
                    
                    if userData > 0:

                        customers.objects.filter(id=userid).update(password=h)
                        customer ={
                        "status":"400",
                        "message":"Password  set Successfully",
                        
                        }
                        return JsonResponse(customer,safe=False)
                    else:
                        customer ={
                                "status":"400",
                                "message":"This user does not exist"  
                                }
                        return JsonResponse(customer,safe=False)
                except:
                    customer ={
                    "status":"400",
                    "message":"Somthing went wrong..",
                    }
                    return JsonResponse(customer,safe=False)
        else: 
            customer ={
                "status":"200",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

#show profile data
def view_profile(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
          
            userdata = json.loads(request.body.decode("utf-8"))
            userid = userdata.get('userid')
            data=[]
            if userid == '':
                customer ={
                    "status":"400",
                    "message":"UserId is Requried",
                }
                return JsonResponse(customer,safe=False)
           
            else: 
                try:
                    userObject = customers.objects.filter(delete='0',id=userid,status=0).count()
                    if userObject > 0 :

                        json_data = json.loads(serializers.serialize( "json",userObject))

                        for j in json_data:
                            data.append(json.loads(json.dumps(j["fields"])))
                        customer ={
                            "status":"200",
                            "userid":userid,    
                            "data":data     }
                        return JsonResponse(customer,safe=False)
                    else:
                        customer ={
                            "status":"400",
                            "userid":userid,  
                            "message":"User not found",  
                            "data":data     }
                        return JsonResponse(customer,safe=False)

                except:
                    customer ={
                    "status":"400",
                    "message":"Somthing went wrong",
                    
                }
                return JsonResponse(customer,safe=False)

        else: 
            customer ={
                "status":"400",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)

#update Profile
def edit_profile(request):
    if request.method == 'POST':
        if request.headers["Apitoken"] == "391a7704a49d9dc3209ced10c55c8dc6":
          
            data = json.loads(request.body.decode("utf-8"))
            userid = data.get('userid')
            fullname=data.get('fullname')
            email = data.get('email')
            dob = data.get('dob')

            customerdata=[]

            if userid == '':
                customer ={
                    "status":"400",
                    "message":"UserId is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif fullname == '':
                customer ={
                    "status":"400",
                    "message":"Fullname is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif dob == '':
                customer ={
                    "status":"400",
                    "message":"Date of Birth is Requried",
                }
                return JsonResponse(customer,safe=False)
            elif email == '':
                customer ={
                    "status":"400",
                    "message":"Email is Requried",
                }
                return JsonResponse(customer,safe=False)
            else: 
                try:
                    userObject = customers.objects.filter(delete='0',id=userid,status='0').count()
                   
                    if userObject > 0 :
                        
                        userProfile=customers.objects.filter(delete='0',id=userid,status=0).update(firstname=fullname,dob=dob,email=email)

                        customer ={
                            "status":"200",
                            "userid":userid,  
                            "message":"Profile Update Successfully" , 
                                 }
                        return JsonResponse(customer,safe=False)
                    else:
                        customer ={
                            "status":"400",
                            "userid":userid,  
                            "message":"User not found"  
                              }
                        return JsonResponse(customer,safe=False)

                except:
                    customer ={
                    "status":"400",
                    "message":"Email is alreay Exits",
                    }
                    return JsonResponse(customer,safe=False)

        else: 
            customer ={
                "status":"400",
                "message":"Authentication failed!!",
            }
            return JsonResponse(customer,safe=False)
    else: 
        customer ={
            "status":"400",
            "message":"Somthing went wrong",
            }
        return JsonResponse(customer,safe=False)
            
            
            
            
    
        