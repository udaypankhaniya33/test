# Generated by Django 4.0.1 on 2022-03-11 06:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20220309_1108'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdetails',
            name='receiverType',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='orderdetails',
            name='senderType',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
