# Generated by Django 3.2.12 on 2022-03-15 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_orderdetails_receivertype_orderdetails_sendertype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdetails',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='delete',
            field=models.SmallIntegerField(default='0'),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
