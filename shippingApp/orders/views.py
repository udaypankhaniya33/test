import base64
from cProfile import Profile

# from datetime import datetime
from distutils.log import error
import email
from multiprocessing import context
from sys import setprofile
from attr import field
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.core import serializers
from accounts.models import Admin_account
from customers.models import customers
from menu.models import menu
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_serverside_datatable.views import ServerSideDatatableView
import requests
import json
import hashlib
from shippingApp.settings import BASE_DIR
from orders.models import orders, Orderdetails
from shippingApp import views
from chatDetails.models import chatDetails
from chats.models import chat

import dateutil.parser

def getAllOrders(request):

    accountId = request.session.get("accountId")
    if accountId is not None:

        mainData = []

        userObject = orders.objects.filter(delete=0)

        UserJs = json.loads((serializers.serialize("json", userObject)))
        # print(UserJs)

        for j in UserJs:

            j["fields"]['userId'] = (json.dumps(j["fields"]['userId']))

            userPic = customers.objects.filter(id=j["fields"]['userId'])
            UserJs = json.loads((serializers.serialize("json", userPic)))

            for k in UserJs:

                j["fields"]["userpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))

            j["fields"]['orderId'] = (json.dumps(j['pk']))
            j["fields"]['transporterId'] = (
                json.dumps(j["fields"]['transporterId']))
            transporterPic = customers.objects.filter(
                id=j["fields"]['transporterId'])
            UserJs = json.loads(
                (serializers.serialize("json", transporterPic)))

            for k in UserJs:

                j["fields"]["transporterpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))

            mainData.append(json.loads(json.dumps(j["fields"])))

        reusable = views.DefaultData(request)
        reusable["mainData"] = mainData

        json_data = reusable
        # print(json_data)

        return render(request, "allOrders.html", json_data)
    else:
        return redirect("/")


def runningOrders(request):

    accountId = request.session.get("accountId")
    if accountId is not None:

        mainData = []

        userObject = orders.objects.filter(delete=0, OrderStatus=1)

        UserJs = json.loads((serializers.serialize("json", userObject)))
        # print(UserJs)

        for j in UserJs:

            j["fields"]['userId'] = (json.dumps(j["fields"]['userId']))
            userPic = customers.objects.filter(id=j["fields"]['userId'])
            UserJs = json.loads((serializers.serialize("json", userPic)))

            for k in UserJs:

                j["fields"]["userpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))
            j["fields"]['orderId'] = (json.dumps(j['pk']))
            j["fields"]['transporterId'] = (
                json.dumps(j["fields"]['transporterId']))
            transporterPic = customers.objects.filter(
                id=j["fields"]['transporterId'])
            UserJs = json.loads(
                (serializers.serialize("json", transporterPic)))

            for k in UserJs:

                j["fields"]["transporterpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))

            mainData.append(json.loads(json.dumps(j["fields"])))

        reusable = views.DefaultData(request)
        reusable["mainData"] = mainData

        json_data = reusable

        return render(request, "RunningOrders.html", json_data)
    else:
        return redirect("/")


def pendingOrders(request):
    accountId = request.session.get("accountId")
    if accountId is not None:

        mainData = []

        userObject = orders.objects.filter(delete=0, OrderStatus=0)

        UserJs = json.loads((serializers.serialize("json", userObject)))
        # print(UserJs)

        for j in UserJs:

            j["fields"]['userId'] = (json.dumps(j["fields"]['userId']))
            userPic = customers.objects.filter(id=j["fields"]['userId'])
            UserJs = json.loads((serializers.serialize("json", userPic)))

            for k in UserJs:

                j["fields"]["userpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))
            j["fields"]['orderId'] = (json.dumps(j['pk']))
            j["fields"]['transporterId'] = (
                json.dumps(j["fields"]['transporterId']))
            transporterPic = customers.objects.filter(
                id=j["fields"]['transporterId'])
            UserJs = json.loads(
                (serializers.serialize("json", transporterPic)))

            for k in UserJs:

                j["fields"]["transporterpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))

            mainData.append(json.loads(json.dumps(j["fields"])))

        reusable = views.DefaultData(request)
        reusable["mainData"] = mainData

        json_data = reusable

        return render(request, "pendingOrders.html", json_data)
    else:
        return redirect("/")



def CompletedOrders(request):
    accountId = request.session.get("accountId")
    if accountId is not None:

        mainData = []

        userObject = orders.objects.filter(delete=0, OrderStatus=2)

        UserJs = json.loads((serializers.serialize("json", userObject)))
        # print(UserJs)

        for j in UserJs:

            j["fields"]['userId'] = (json.dumps(j["fields"]['userId']))
            userPic = customers.objects.filter(id=j["fields"]['userId'])
            UserJs = json.loads((serializers.serialize("json", userPic)))

            for k in UserJs:

                j["fields"]["userpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))
            j["fields"]['orderId'] = (json.dumps(j['pk']))
            j["fields"]['transporterId'] = (
                json.dumps(j["fields"]['transporterId']))
            transporterPic = customers.objects.filter(
                id=j["fields"]['transporterId'])
            UserJs = json.loads(
                (serializers.serialize("json", transporterPic)))

            for k in UserJs:

                j["fields"]["transporterpic"] = json.loads(
                    json.dumps(k["fields"]['profilepic']))
                # mainData.append(json.loads(json.dumps(j["fields"])))

            mainData.append(json.loads(json.dumps(j["fields"])))

        reusable = views.DefaultData(request)
        reusable["mainData"] = mainData

        json_data = reusable

        return render(request, "pendingOrders.html", json_data)
    else:
        return redirect("/")



def orderDetail(request, orderId):

    transporterArr = []
    ChatDataArr=[]
    OrderData=[]
    accountId = request.session.get("accountId")
    if accountId is not None:
        base64_bytes = orderId.encode("ascii")
        sample_string_bytes = base64.b64decode(base64_bytes)
        OrderId = (sample_string_bytes.decode("ascii"))
        
        
        
        Chats = Orderdetails.objects.all().filter(orderid=OrderId)
        chatJs = json.loads((serializers.serialize("json", Chats)))
        reusable = views.DefaultData(request)
        
  
        for j in chatJs:
            # print(j["fields"]["messagetime"])
            
            # print("date_string =", date_string)
            # print("type of date_string =", type(date_string))

            j["fields"]["created"]= (dateutil.parser.isoparse(j["fields"]["created"])).strftime("%H:%M %p %b %d")
            
            print(j["fields"]["created"])
            # print(FormentedData)
            
            OrderData.append(j["fields"])
            
            
        reusable["ChatData"]=OrderData

        return render(request, "AboutOrder.html", reusable)

    else:
        return redirect("/")




def orderChats(request,orderId):
    accountId = request.session.get("accountId")
    if accountId is not None:
        
        transporterArr=[]
        base64_bytes = orderId.encode("ascii")
        sample_string_bytes = base64.b64decode(base64_bytes)
        OrderId = (sample_string_bytes.decode("ascii"))
        chats=chat.objects.filter(orderId=OrderId,delete=0)
        chatJs = json.loads((serializers.serialize("json", chats)))
        
        for k in chatJs:
            # print(k)
            transporters = customers.objects.filter(id=(k["fields"]["transporterId"]))
            transporterData = json.loads(
                (serializers.serialize("json", transporters)))
            for user in transporterData:
                # print(user)
                k["fields"]["userPic"]=(user["fields"]["profilepic"])
                k["fields"]["UserfullName"]=(user["fields"]["firstname"])+" ."+((user["fields"]["lastname"])[0]).upper() 
                k["fields"]["transporterId"]=json.dumps(k["fields"]["transporterId"])
                k["fields"]["chatId"]=json.dumps(k["pk"])
                
            transporterArr.append(k["fields"])    
               
            
         
          
            
        reusable = views.DefaultData(request)
        reusable["chatsHeadings"]= transporterArr
        # print( reusable["chatsHeadings"])
        
        return render(request, "OrderChats.html", reusable)        
    else:
        return redirect("/")
    
    
    
    
    
    


def getChats(request):
    accountId = request.session.get("accountId")

    if accountId is not None:

        if request.method == 'POST':
            # transporterId = request.POST["OrderDetailId"]
            transporterArr=[]
            dataArr=[]
            chatsArr = {"data": dataArr,
                    "transporterData": transporterArr,                  
                    }       
            var = json.loads(json.dumps(request.POST["data"]))
            data = json.loads(var)
            # print(data)
            
            chatData=chatDetails.objects.filter(chatId=data["chatId"],delete=0).order_by("created")
            serializeJs=json.loads((serializers.serialize("json", chatData)))
            for j in serializeJs:
                j["fields"]["created"]= (dateutil.parser.isoparse(j["fields"]["created"])).strftime("%H:%M %p %b %d")
                dataArr.append(j["fields"])
            
            tranporter=chat.objects.filter(id=data["chatId"],delete=0)
            transporterSerialiser=json.loads((serializers.serialize("json", tranporter)))
            for j in transporterSerialiser:
                # print(j["fields"])
                transporterDetails=customers.objects.filter(id=j["fields"]["transporterId"])
                js=json.loads((serializers.serialize("json", transporterDetails)))
                for k in js :
                    
                    transporterArr.append(k["fields"])
            
            
            
        
               
                print(chatsArr)

            


        return render(request, "ajaxChat.html",chatsArr)
    else:
        return redirect("/")


